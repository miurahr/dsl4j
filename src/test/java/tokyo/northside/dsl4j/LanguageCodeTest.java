package tokyo.northside.dsl4j;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import tokyo.northside.dsl4j.data.LanguageCode;

class LanguageCodeTest {
    @Test
    void constructor() {
        LanguageCode languageCode = new LanguageCode();
        assertEquals("en", languageCode.get(1));
    }
}
