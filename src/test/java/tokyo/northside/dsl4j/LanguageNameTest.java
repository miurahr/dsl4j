package tokyo.northside.dsl4j;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import tokyo.northside.dsl4j.data.LanguageName;

class LanguageNameTest {

    @Test
    void constructor() {
        LanguageName languageName = new LanguageName();
        assertEquals("en", languageName.get("English"));
    }
}
