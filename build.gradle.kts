import java.io.File
import java.io.FileInputStream
import java.util.Properties

plugins {
    jacoco
    signing
    `java-library`
    `java-library-distribution`
    `maven-publish`
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.nexus.publish)
    alias(libs.plugins.git.version) apply false
    alias(libs.plugins.javacc)
    alias(libs.plugins.protobuf)
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.jetbrains.annotations)
    implementation(libs.trie4j)
    implementation(libs.commons.io)
    implementation(libs.dictzip)
    implementation(libs.protobuf)
    testImplementation(libs.groovy.all)
    testImplementation(libs.junit.jupiter)
}

tasks.wrapper {
    distributionType = Wrapper.DistributionType.BIN
    gradleVersion = "8.10"
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

spotbugs {
    excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    tasks.spotbugsMain {
        reports.create("html") {
            required.set(true)
        }
    }
    tasks.spotbugsTest {
        reports.create("html") {
            required.set(true)
        }
    }
}

jacoco {
    toolVersion = libs.versions.jacoco.get()
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(false)
        html.required.set(true)
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
    withSourcesJar()
    withJavadocJar()
}

tasks.jar {
    manifest {
        attributes("Automatic-Module-Name" to "tokyo.northside.dsl4j")
    }
}

javacc {
    javaCCVersion = libs.versions.javacc.get()
    configs.create("dsl") {
        inputFile = File("src/main/java/tokyo/northside/dsl4j/DslParser.jj")
        packageName = "tokyo.northside.dsl4j"
    }
}

sourceSets {
    main {
        java {
            srcDir(javacc.configs.getByName("dsl").outputDir)
            srcDir(layout.buildDirectory.file("generated/source/proto/main/java"))
        }
    }
}

tasks.withType<Javadoc> {
    include("tokyo/northside/dsl4j/index/*")
    exclude("tokyo/northside/dsl4j/DslParser*",
            "tokyo/northside/dsl4j/Token*",
            "tokyo/northside/dsl4j/JavaCharStream.java",
            "tokyo/northside/dsl4j/ParseException.java",
            "tokyo/northside/dsl4j/DslIndexOuterClass.java",
            "tokyo/northside/dsl4j/index/DslIndexOrBuilder.java",
            "tokyo/northside/dsl4j/index/DslIndex.java"
    )
}

val protobufVersion = libs.versions.protobuf.get()
protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }
}

val dotgit = project.file(".git")
if (dotgit.exists()) {
    apply(plugin = libs.plugins.git.version.get().pluginId)
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = when {
        details.isCleanTag -> baseVersion
        else -> baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }
} else {
    val gitArchival = project.file(".git-archival.properties")
    val props = Properties()
    props.load(FileInputStream(gitArchival))
    val versionDescribe = props.getProperty("describe")
    val regex = "^v\\d+\\.\\d+\\.\\d+$".toRegex()
    version = when {
        regex.matches(versionDescribe) -> versionDescribe.substring(1)
        else -> versionDescribe.substring(1) + "-SNAPSHOT"
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            pom {
                name.set("dsl4j")
                description.set("LingoDSL parser for java")
                url.set("https://codeberg.org/miurahr/dsl4j")
                licenses {
                    license {
                        name.set("The GNU General Public License, Version 3")
                        url.set("https://www.gnu.org/licenses/licenses/gpl-3.html")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("miurahr")
                        name.set("Hiroshi Miura")
                        email.set("miurahr@linux.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://codeberg.org/miurahr/dsl4j.git")
                    developerConnection.set("scm:git:git://codeberg.org/miurahr/dsl4j.git")
                    url.set("https://codeberg.org/miurahr/dsl4j")
                }
            }
        }
    }
}

signing {
    if (project.hasProperty("signingKey")) {
        val signingKey: String? by project
        val signingPassword: String? by project
        useInMemoryPgpKeys(signingKey, signingPassword)
    } else {
        useGpgCmd()
    }
    sign(publishing.publications["mavenJava"])
}

tasks.withType<Sign> {
    val hasKey = project.hasProperty("signingKey") || project.hasProperty("signing.gnupg.keyName")
    onlyIf { hasKey && !project.version.toString().endsWith("-SNAPSHOT") }
}

// ---------- publish to sonatype OSSRH
val sonatypeUsername: String? by project
val sonatypePassword: String? by project
nexusPublishing {
    repositories.sonatype {
        if (sonatypeUsername != null && sonatypePassword != null) {
            username.set(sonatypeUsername)
            password.set(sonatypePassword)
        } else {
            username.set(System.getenv("SONATYPE_USER"))
            password.set(System.getenv("SONATYPE_PASS"))
        }
    }
}

tasks.withType<Copy> {
    duplicatesStrategy = DuplicatesStrategy.WARN
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}

spotless {
    format("misc") {
        target(listOf("*.gradle", ".gitignore"))
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    java {
        target(listOf("src/*/java/**/*.java"))
        palantirJavaFormat()
        importOrder()
        removeUnusedImports()
        formatAnnotations()
    }
}